# Test task for timeweb.ru #

Specification: see specification.jpg. 

Original estimate: 8-16 hours, elapsed time: ~10 hours.
 
See [CHANGELOG](/CHANGELOG.md). 

## Used libraries and tools:
    
* docker-compose for orchestration 
* php-fpm + pdo_mysql extension
* web server nginx
* database mysql 
* composer for autoloading
* bobthecow/mustache.php as template engine 
* jQuery for ajax requests and other interactive features
* jQuery Form Validator for client side validation (html5 attributes used too)
* bootstrap for styling

## Requirements

1. composer
2. docker 1.13.1+ with docker-compose
3. free 8080 port (can be changed in docker-compose.yml)

## Install and run:

1. `git clone git@bitbucket.org:samizdam/tw.test.git`
2. `cd tw.test/`
3. `composer install`
4. `docker-compose up -d`
5. `cat init.sql | docker exec -i twtest_db_1 mysql tw_test`
6. Open application [http://127.0.0.1:8080](http://127.0.0.1:8080)

## Testing

Run 
`./phpunit.sh`

Coverage report available in [tests/coverage](/tests/coverage/index.html) 
