CREATE TABLE IF NOT EXISTS `search_result` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `url` VARCHAR(1024) NOT NULL,
    `type` ENUM('link','img','text') NOT NULL,
    `items` TEXT NOT NULL,
    `number_of_items` INT(10) UNSIGNED NOT NULL,
    `create_timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
