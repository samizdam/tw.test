# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [2.0.0] - 2017-10-01
### Changed
- Use docker-compose for run application: include nginx, php-fpm and mysql images to project. 
- Common php code refactoring. 
- Add unit tests for most impotent classes. 
- Fix some bugs. 

## [1.0.0] - 2014-12-14
### Added
- Full implementation. 