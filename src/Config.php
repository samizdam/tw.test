<?php

namespace Samizdam\TimewebTestSearchModule;

class Config
{
    private $config;

    public function __construct(array $config)
    {
        $this->config = new \ArrayObject($config, \ArrayObject::ARRAY_AS_PROPS);
    }

    public function getViewPath()
    {
        return $this->config['viewPath'];
    }

    public function getDbDetails($name)
    {
        return $this->config['db'][$name];
    }
}