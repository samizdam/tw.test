<?php

namespace Samizdam\TimewebTestSearchModule\Exception;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class RuntimeException extends \RuntimeException implements SearchModuleExceptionInterface
{

}