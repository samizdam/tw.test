<?php

namespace Samizdam\TimewebTestSearchModule\Exception;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class DomainException extends \DomainException implements SearchModuleExceptionInterface
{

}