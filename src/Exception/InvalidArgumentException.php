<?php

namespace Samizdam\TimewebTestSearchModule\Exception;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class InvalidArgumentException extends \InvalidArgumentException implements SearchModuleExceptionInterface
{

}