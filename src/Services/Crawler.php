<?php

namespace Samizdam\TimewebTestSearchModule\Services;

use Samizdam\TimewebTestSearchModule\Exception\RuntimeException;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class Crawler
{
    public function grabContent(string $url): string
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $subject = curl_exec($ch);
        if ($subject === false) {
            $message = sprintf('Can not connect to url `%s`. ', $url);
            throw new RuntimeException($message, curl_errno($ch));
        }

        return $subject;
    }
}