<?php

namespace Samizdam\TimewebTestSearchModule\Models;

class ItemFactory
{
    public function factory($type, $url, $content): BaseItem
    {
        $itemClass = $this->getClassByType($type);
        return new $itemClass($url, $content);
    }

    protected function getClassByType($type): string
    {
        return __NAMESPACE__ . '\\' . ucfirst($type) . 'Item';
    }
}