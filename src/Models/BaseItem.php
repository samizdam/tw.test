<?php

namespace Samizdam\TimewebTestSearchModule\Models;

abstract class BaseItem extends BaseModel
{

    const TYPE_TEXT = 'text';

    const TYPE_LINK = 'link';

    const TYPE_IMG = 'img';

    protected $url;

    protected $content;

    public function __construct(string $url, string $content)
    {
        $this->url = $url;
        $this->content = $content;
    }

    public function getType(): string
    {
        return static::TYPE;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function __toString()
    {
        return $this->getContent();
    }
}