<?php

namespace Samizdam\TimewebTestSearchModule\Models;

class LinkItem extends BaseItem
{
    const PATTERN_SRC = '/href(?:[=\s]+["\'])(.*?)(?:["\'])/im';

    const TYPE = self::TYPE_LINK;

    public function getContent(): string
    {
        preg_match(self::PATTERN_SRC, $this->content, $matches);
        $src = $matches[1];
        foreach (['http://', 'https://', '//'] as $schema) {
            if (stripos($src, $schema) === 0) {
                $schemaExist = true;
            }
        }

        if (empty($schemaExist)) {
            $baseUrl = '//' . parse_url($this->url, PHP_URL_HOST) . $src;
            return str_replace($src, $baseUrl, $this->content);
        } else {
            return $this->content;
        }
    }
}