<?php

namespace Samizdam\TimewebTestSearchModule\Models;

use Samizdam\TimewebTestSearchModule\Exception\DomainException;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class ItemsRepository
{

    /**
     * @var \PDO
     */
    private $pdoConnection;

    public function __construct(\PDO $pdoConnection)
    {
        $this->pdoConnection = $pdoConnection;
    }

    public function getPagesTotalCounts(): array
    {
        $statement = $this->pdoConnection->prepare('select 
					sr.url,
					SUM(sr.number_of_items) as `totalNumberOfItems`
					from search_result sr
					group by sr.url'
        );

        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getAllByUrl(string $url): array
    {
        $statement = $this->pdoConnection->prepare('select id from `search_result`  where 1 and `url` = :url');
        $statement->execute([':url' => $url]);
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $collections = [];
        foreach ($rows as $i => $row) {
            $collection = $this->getCollectionById($row['id']);
            $collections[$i] = $collection;
        }
        return $collections;
    }

    public function getCollectionById($id): ItemsCollection
    {
        $statement = $this->pdoConnection->prepare('select * from `search_result`  where 1 and `id` = :id limit 1');
        $statement->execute([':id' => $id]);
        $row = $statement->fetch(\PDO::FETCH_ASSOC);

        if (count($row)) {
            $items = unserialize($row['items']);
            $collection = new ItemsCollection($row['url'], $row['type'], $items, $row['id'], $row['create_timestamp']);
            return $collection;
        }

        throw new DomainException(sprintf('Items collection with id `%s` not found. ', $id));
    }

    public function save(ItemsCollection $itemsCollection)
    {
        $statement = $this->pdoConnection->prepare('insert into `search_result` set 
				`url` = :url,
				`items` = :items,
				`type` = :type,
				`number_of_items` = :numberOfItems
				');

        $statement->execute([
            ':url' => $itemsCollection->getUrl(),
            ':items' => serialize((array)$itemsCollection),
            ':type' => $itemsCollection->getType(),
            ':numberOfItems' => $itemsCollection->count(),
        ]);
    }
}