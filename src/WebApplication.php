<?php

namespace Samizdam\TimewebTestSearchModule;

use Samizdam\TimewebTestSearchModule\Controllers\ControllerFactory;

class WebApplication
{
    const DEFULT_CONTROLLER_NAME = 'main';
    const DEFULT_ACTION_NAME = 'index';

    /**
     * @var ControllerFactory
     */
    private $controllerFactory;

    public function __construct(ControllerFactory $controllerFactory)
    {
        $this->controllerFactory = $controllerFactory;
    }

    public function processRequest(Request $request)
    {
        $controllerName = $request->get('c', self::DEFULT_CONTROLLER_NAME);
        $controller = $this->controllerFactory->createController($controllerName);

        $actionName = $request->get('a', self::DEFULT_ACTION_NAME);
        $response = $controller->getResponse($actionName, $request);

        $this->sendResponse($response);
    }

    protected function sendResponse(Response $response)
    {
        http_response_code($response->getStatus());
        echo $response->getBody();
    }
}