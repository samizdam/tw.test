<?php

namespace Samizdam\TimewebTestSearchModule;

class Response
{
    const STATUS_CODE_OK = 200;

    const STATUS_CODE_BAD_REQUEST = 400;
    const STATUS_CODE_NOT_FOUND = 404;

    protected $status = self::STATUS_CODE_OK;

    public function setStatus($code)
    {
        $this->status = $code;
    }

    public function getStatus()
    {
        return $this->status;
    }

    protected $body = '';

    public function setBody($string)
    {
        $this->body = $string;
    }

    public function getBody()
    {
        return $this->body;
    }


}