<?php

namespace Samizdam\TimewebTestSearchModule\Controllers;

use FreeElephants\DI\Injector;
use Samizdam\TimewebTestSearchModule\Config;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class ControllerFactory
{

    /**
     * @var Injector
     */
    private $injector;
    /**
     * @var Config
     */
    private $config;

    public function __construct(Injector $injector, Config $config)
    {
        $this->injector = $injector;
        $this->config = $config;
    }

    public function createController(string $controllerName): BaseController
    {
        $controllerClassName = __NAMESPACE__ . '\\' . ucfirst($controllerName);
        /**@var $controller BaseController */
        $controller = $this->injector->createInstance($controllerClassName);
        $controller->setTemplateEngine(new \Mustache_Engine([
            'loader' => new \Mustache_Loader_FilesystemLoader($this->config->getViewPath(),
                ['extension' => '.html']),
            'partials_loader' => new \Mustache_Loader_FilesystemLoader($this->config->getViewPath() . '/partials',
                ['extension' => '.html']),
        ]));
        return $controller;
    }
}