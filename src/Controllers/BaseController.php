<?php

namespace Samizdam\TimewebTestSearchModule\Controllers;

use Samizdam\TimewebTestSearchModule\Request;
use Samizdam\TimewebTestSearchModule\Response;

abstract class BaseController
{

    /**
     * @var \Mustache_Engine
     */
    protected $mustache;

    /**
     *
     * @var Request
     */
    protected $request;

    /**
     *
     * @var Response
     */
    protected $response;

    public function setTemplateEngine(\Mustache_Engine $engine)
    {
        $this->mustache = $engine;
    }

    public function getResponse(string $actionName, Request $request): Response
    {
        $this->response = new Response();
        $this->request = $request;
        $action = $this->routeAction($actionName);
        if (method_exists($this, $action)) {
            $this->response->setBody($this->{$action}());
        } else {
            $this->response->setStatus(Response::STATUS_CODE_NOT_FOUND);
        }
        return $this->response;
    }

    protected function routeAction(string $actionName)
    {
        $action = 'action' . ucfirst($actionName);
        return $action;
    }

    protected function renderBlock(string $name, array $data = [])
    {
        return $this->mustache->render('partials/' . $name, $data);
    }

    protected function renderPage(array $data = [])
    {
        return $this->mustache->render('layout', $data);
    }
}