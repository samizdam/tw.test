<?php

namespace Samizdam\TimewebTestSearchModule\Controllers;

use Samizdam\TimewebTestSearchModule\Models\ItemsRepository;

class Archive extends BaseController
{

    /**
     * @var ItemsRepository
     */
    private $itemsRepository;

    public function __construct(ItemsRepository $itemsRepository)
    {
        $this->itemsRepository = $itemsRepository;
    }

    protected function actionIndex()
    {
        $pageList = '';
        foreach ($this->itemsRepository->getPagesTotalCounts() as $page) {
            $pageList .= $this->renderBlock('pageTotal', $page);
        }
        $data = $this->renderBlock('archive', ['pageList' => $pageList]);
        return $this->renderPage(['content' => $data]);
    }

    protected function actionDetails()
    {
        $url = $this->request->get('url');
        $collectionList = '';
        foreach ($this->itemsRepository->getAllByUrl($url) as $i => $collection) {
            $collectionList .= $this->renderBlock('collectionSummary', ['collection' => $collection]);
        }
        $data = $this->renderBlock('collectionList', ['collectionList' => $collectionList]);
        return $this->renderPage(['content' => $data]);
    }

    protected function actionItemsCollection()
    {
        $collectionId = $this->request->get('collectionId');
        $collection = $this->itemsRepository->getCollectionById($collectionId);

        $html = '';
        $iterator = $collection->getIterator();
        while ($iterator->valid()) {
            $html .= $this->renderBlock('item', [
                'itemContent' => $iterator->current(),
                'itemNumber' => $iterator->key() + 1,
                'itemType' => $collection->getType(),
            ]);
            $iterator->next();
        }

        return $html;

    }
}