<?php

namespace Samizdam\TimewebTestSearchModule\Controllers;

use Samizdam\TimewebTestSearchModule\Models\ItemsRepository;
use Samizdam\TimewebTestSearchModule\Services\ItemsFinder;

class Search extends BaseController
{
    /**
     * @var ItemsFinder
     */
    private $itemsFinder;
    /**
     * @var ItemsRepository
     */
    private $itemsRepository;

    public function __construct(ItemsFinder $itemsFinder, ItemsRepository $itemsRepository)
    {
        $this->itemsFinder = $itemsFinder;
        $this->itemsRepository = $itemsRepository;
    }

    public function actionIndex()
    {
        $data = ['results' => ''];

        $url = $this->request->get('searchUrl');
        $type = $this->request->get('searchType');
        $text = $this->request->get('searchText');

        $itemsCollection = $this->itemsFinder->find($url, $type, $text);
        foreach ($itemsCollection as $num => $item) {
            $data['results'] .= $this->renderBlock('item',
                [
                    'itemNumber' => $num + 1,
                    'itemContent' => $item,
                    'itemType' => $type,
                ]
            );
        }
        $data['numberOfItems'] = $itemsCollection->count();
        $this->itemsRepository->save($itemsCollection);
        return $this->renderBlock('resultArea', $data);
    }

}