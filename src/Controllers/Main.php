<?php

namespace Samizdam\TimewebTestSearchModule\Controllers;

class Main extends BaseController
{
    public function actionIndex()
    {
        $data = [];
        $data['content'] = $this->renderBlock('form');
        return $this->renderPage($data);
    }
}