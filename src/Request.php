<?php

namespace Samizdam\TimewebTestSearchModule;

class Request
{

    protected $method;

    protected $server = [];
    protected $get = [];
    protected $post = [];


    public function __construct(array $server = [], array $get = [], array $post = [])
    {
        $this->method = $server['REQUEST_METHOD'];
        $this->get = $get;
        $this->post = $post;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function get($name = null, $default_value = null)
    {
        return $this->getVariable($this->get, $name, $default_value);
    }

    public function post($name = null, $default_value = null)
    {
        return $this->getVariable($this->post, $name, $default_value);
    }

    protected function getVariable(array $array = [], $name = null, $default_value = null)
    {
        if (empty($name)) {
            return $array;
        } else {
            return isset($array[$name])
                ? $array[$name]
                : $default_value;
        }

    }

}