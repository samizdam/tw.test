#!/usr/bin/env bash

docker run --rm \
    -v $(pwd)/src:/code/src:ro \
    -v $(pwd)/vendor:/code/vendor:ro \
    -v $(pwd)/phpunit.xml.dist:/code/phpunit.xml.dist \
    -v $(pwd)/tests:/code/tests \
    --user $UID:$UID \
    -w /code \
    tw_test/php vendor/bin/phpunit $@