<?php
require_once '../vendor/autoload.php';

use FreeElephants\DI\Injector;
use FreeElephants\DI\InjectorBuilder;
use Samizdam\TimewebTestSearchModule\Request;
use Samizdam\TimewebTestSearchModule\WebApplication;

$components = require __DIR__ . '/../config/components.php';
$di = (new InjectorBuilder())->buildFromArray($components);
// register itself for simplify usage in factories
$di->setService(Injector::class, $di);

$webApplication = $di->getService(WebApplication::class);
/**@var WebApplication $webApplication */
$webApplication->processRequest(new Request($_SERVER, $_GET, $_POST));
