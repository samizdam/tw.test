<?php

namespace Samizdam\TimewebTestSearchModuleTest\Services;

use PHPUnit\Framework\TestCase;
use Samizdam\TimewebTestSearchModule\Exception\RuntimeException;
use Samizdam\TimewebTestSearchModule\Services\Crawler;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class CrawlerTest extends TestCase
{

    public function testGrabContent_exception()
    {
        $crawler = new Crawler();
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Can not connect to url `http://foo`bar`. ');
        $crawler->grabContent('http://foo`bar');
    }
}