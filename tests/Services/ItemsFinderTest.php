<?php

namespace Samizdam\TimewebTestSearchModuleTest\Services;

use PHPUnit\Framework\TestCase;
use Samizdam\TimewebTestSearchModule\Exception\InvalidArgumentException;
use Samizdam\TimewebTestSearchModule\Services\Crawler;
use Samizdam\TimewebTestSearchModule\Services\ItemsFinder;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class ItemsFinderTest extends TestCase
{

    const EXAMPLE_COM_CONTENT = <<<HTML
<!doctype html>
<html>
<head>
    <title>Example Domain</title>

    <meta charset="utf-8" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <style type="text/css">
    body {
        background-color: #f0f0f2;
        margin: 0;
        padding: 0;
        font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
        
    }
    div {
        width: 600px;
        margin: 5em auto;
        padding: 50px;
        background-color: #fff;
        border-radius: 1em;
    }
    a:link, a:visited {
        color: #38488f;
        text-decoration: none;
    }
    @media (max-width: 700px) {
        body {
            background-color: #fff;
        }
        div {
            width: auto;
            margin: 0 auto;
            border-radius: 0;
            padding: 1em;
        }
    }
    </style>    
</head>

<body>
<div>
    <h1>Example Domain</h1>
    <p>This domain is established to be used for illustrative examples in documents. You may use this
    domain in examples without prior coordination or asking for permission.</p>
    <p><a href="http://www.iana.org/domains/example">More information...</a></p>
</div>
</body>
</html>
HTML;


    public function testFind_text()
    {
        $finder = new ItemsFinder($this->createCrawlerMock());
        $resultCollection = $finder->find('http://example.com/', 'text', 'This domain is established');
        $this->assertCount(1, $resultCollection);
    }

    public function testFind_invalid_url()
    {
        $finder = new ItemsFinder($this->createCrawlerMock());
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid url `foo`bar`. ');
        $finder->find('foo`bar', 'link');
    }

    public function testFind_unsupported_type()
    {
        $finder = new ItemsFinder($this->createCrawlerMock());
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Unsupported search subject type `unsupported-type`. Supported types are: img, link, text. ');
        $finder->find('http://example.com/', 'unsupported-type');
    }

    public function testFind_empty_text_pattern()
    {
        $finder = new ItemsFinder($this->createCrawlerMock());
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Search pattern is empty. ');
        $finder->find('http://example.com/', 'text', '');
    }

    private function createCrawlerMock(): Crawler
    {
        $crawler = $this->createMock(Crawler::class);
        $crawler->method('grabContent')->willReturn(self::EXAMPLE_COM_CONTENT);
        /**@var Crawler $crawler */
        return $crawler;
    }
}