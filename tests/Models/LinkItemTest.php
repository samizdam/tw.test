<?php

namespace Samizdam\TimewebTestSearchModuleTest\Models;

use PHPUnit\Framework\TestCase;
use Samizdam\TimewebTestSearchModule\Models\LinkItem;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class LinkItemTest extends TestCase
{

    public function testGetContent()
    {
        $linkItem = new LinkItem('http://example.com', '<a href="http://www.iana.org/domains/example">More information...</a>');
        $this->assertSame('<a href="http://www.iana.org/domains/example">More information...</a>', $linkItem->getContent());
    }

    public function testGetContent_with_empty_schema()
    {
        $linkItem = new LinkItem('http://example.com', '<a href="/domains/example">More information...</a>');
        $this->assertSame('<a href="//example.com/domains/example">More information...</a>', $linkItem->getContent());
    }

    public function testGetType()
    {
        $linkItem = new LinkItem('http://example.com', '<a href="/domains/example">More information...</a>');
        $this->assertSame('link', $linkItem->getType());
    }
}