<?php

namespace Samizdam\TimewebTestSearchModuleTest\Models;

use PHPUnit\Framework\TestCase;
use Samizdam\TimewebTestSearchModule\Models\ImgItem;
use Samizdam\TimewebTestSearchModule\Models\ItemFactory;
use Samizdam\TimewebTestSearchModule\Models\LinkItem;
use Samizdam\TimewebTestSearchModule\Models\TextItem;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class ItemFactoryTest extends TestCase
{

    public function testFactory()
    {
        $factory = new ItemFactory();
        $this->assertInstanceOf(ImgItem::class,  $factory->factory('img', 'http://example.com', '<img src="/some.png"/>'));
        $this->assertInstanceOf(LinkItem::class,  $factory->factory('link', 'http://example.com', '<a href="http://www.iana.org/domains/example">More information...</a>'));
        $this->assertInstanceOf(TextItem::class,  $factory->factory('text', 'http://example.com', 'Some text'));
    }

}