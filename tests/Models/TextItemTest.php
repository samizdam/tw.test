<?php

namespace Samizdam\TimewebTestSearchModuleTest\Models;

use PHPUnit\Framework\TestCase;
use Samizdam\TimewebTestSearchModule\Models\TextItem;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class TextItemTest extends TestCase
{

    public function testGetUrl()
    {
        $textItem = new TextItem('http://example.com', 'Some text');
        $this->assertSame('http://example.com', $textItem->getUrl());
    }

    public function testGetType()
    {
        $textItem = new TextItem('http://example.com', 'Some text');
        $this->assertSame('text', $textItem->getType());
    }

    public function testGetContent()
    {
        $textItem = new TextItem('http://example.com', 'Some text');
        $this->assertSame('Some text', $textItem->getContent());
    }

    public function testToString()
    {
        $textItem = new TextItem('http://example.com', 'Some text');
        $this->assertSame('Some text', (string) $textItem);
    }
}