<?php

namespace Samizdam\TimewebTestSearchModuleTest\Models;

use PHPUnit\Framework\TestCase;
use Samizdam\TimewebTestSearchModule\Models\ImgItem;

/**
 * @author samizdam <samizdam@inbox.ru>
 */
class ImgItemTest extends TestCase
{

    public function testGetContent_with_absolute_url()
    {
        $imgItem = new ImgItem('http://example.com', '<img src="http://example.com/some.png"/>');
        $this->assertSame('<img src="http://example.com/some.png"/>', $imgItem->getContent());
    }

    public function testGetContent_without_scheme()
    {
        $imgItem = new ImgItem('http://example.com', '<img src="/some.png"/>');
        $this->assertSame('<img src="//example.com/some.png"/>', $imgItem->getContent());
    }

    public function testGetType()
    {
        $imgItem = new ImgItem('http://example.com', '<img src="/some.png"/>');
        $this->assertSame('img', $imgItem->getType());
    }

}