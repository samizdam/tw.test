<?php
use Samizdam\TimewebTestSearchModule\Config;
use Samizdam\TimewebTestSearchModule\Controllers\ControllerFactory;
use Samizdam\TimewebTestSearchModule\Models\ItemsRepository;
use Samizdam\TimewebTestSearchModule\Services\Crawler;
use Samizdam\TimewebTestSearchModule\Services\ItemsFinder;
use Samizdam\TimewebTestSearchModule\WebApplication;

$config = new Config(require_once __DIR__ . '/../config/config.php');

return [
    'register' => [
        WebApplication::class,
        ControllerFactory::class,
        Crawler::class,
        ItemsFinder::class,
        ItemsRepository::class,
    ],
    'instances' => [
        \PDO::class => new \PDO($config->getDbDetails('dns'), $config->getDbDetails('username'),
            $config->getDbDetails('password'), [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]),
        Config::class => $config,
    ],
];