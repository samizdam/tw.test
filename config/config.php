<?php
return [
	'db' => [
		'dns' => 'mysql:dbname=tw_test;host=db;port=3306',
		'username' => 'tw_test',
		'password' => 'tw_test_password',
	],
	'viewPath' => __DIR__ . '/../src/views',
];